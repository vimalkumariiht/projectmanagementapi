﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DL
{
    public class User
    {
        [Key]
        [Column("User_ID")]
        public int UserId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        [Column("Employee_ID")]
        public string EmployeeId { get; set; }

        [Column("Project_ID")]
        public int? ProjectId { get; set; }

        public Project Project { get; set; }

        public List<Task> Tasks { get; set; }

        public List<Project> ManagerUsers { get; set; }
    }
}