﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DL
{
    public class Task
    {
        [Key]
        [Column("Task_ID")]
        public int TaskId { get; set; }

        [Column("Parent_ID")]
        public int? ParentId { get; set; }
        public Task ParentTask { get; set; }

        [Column("Project_ID")]
        public int? ProjectId { get; set; }
        public Project Project { get; set; }

        [Column("User_ID")]
        public int? UserId { get; set; }
        public User User { get; set; }

        [Column("TaskDescription")]
        public string TaskDescription { get; set; }
        [Column("Start_Date")]
        public DateTime? StartDate { get; set; }
        [Column("End_Date")]
        public DateTime? EndDate { get; set; }
        [Column("Priority")]
        public int? Priority { get; set; }
        public List<Task> Tasks { get; set; }
    }
}
