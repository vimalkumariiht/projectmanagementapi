﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DL
{
    public class Project
    {
        [Key]
        [Column("Project_ID")]
        public int ProjectId { get; set; }
         
        [Column("Project")]
        public string ProjectName { get; set; }
        
        public DateTime? StartDate { get; set; }
         
        public DateTime? EndDate { get; set; }
         
        public int? Priority { get; set; }

        public int? ManagerId { get; set; }

        public User ManagerUser { get; set; }
        
        public List<Task> Tasks { get; set; }

        public List<User> Users { get; set; }
    }
}
