﻿using System.Data.Entity;

namespace DL
{
    public class ProjectManagerContext : DbContext
    {
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Task>()
                .HasOptional(s => s.ParentTask)
                .WithMany(g => g.Tasks)
                .HasForeignKey(s => s.ParentId);

            modelBuilder.Entity<Task>()
                .HasOptional(s => s.Project)
                .WithMany(g => g.Tasks)
                .HasForeignKey(s => s.ProjectId);

            modelBuilder.Entity<Task>()
                .HasOptional(s => s.User)
                .WithMany(g => g.Tasks)
                .HasForeignKey(s => s.UserId);

            modelBuilder.Entity<User>()
                .HasOptional(s => s.Project)
                .WithMany(g => g.Users)
                .HasForeignKey(s => s.ProjectId);

            modelBuilder.Entity<Project>()
                .HasOptional(s => s.ManagerUser)
                .WithMany(a => a.ManagerUsers)
                .HasForeignKey(b => b.ManagerId);
        }
    }
}