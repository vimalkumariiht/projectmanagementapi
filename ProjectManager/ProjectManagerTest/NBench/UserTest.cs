﻿using BL.Contracts;
using BL.Model;
using Moq;
using NBench;
using ProjectManagerApi.Controllers;
using System.Collections.Generic;

namespace ProjectManagerTest.NBench
{
    public class UserTest
    {
        private int userId = 0;
        UserController controller;
        private const string AddUserCounter = "AddUserCounter";
        private const string UpdateUserCounter = "UpdateUserCounter";
        private const string SearchUserCounter = "SearchUserCounter";
        private const int AcceptableMinThroughput = 100;
        private Counter addUserCounter;
        private Counter updateUserCounter;
        private Counter searchUserCounter; 

        [PerfSetup]
        public void Setup()
        {
            controller = new UserController();
            var mockService = new Mock<IUserOperation>();
            UserModel t = new UserModel() { FirstName = "User1", LastName = "kumar", EmployeeId = "Emp001", ProjectId = 1 };
            mockService.Setup(x => x.AddUser(t))
                    .Returns<UserModel>(r => new UserModel() { UserId = 2, FirstName = "User2", LastName = "kumar", EmployeeId = "Emp001", ProjectId = 1 });
            mockService.Setup(y => y.GetUser())
                .Returns(new List<UserModel>() { new UserModel() { UserId = 2, FirstName = "User2", LastName = "kumar", EmployeeId = "Emp001", ProjectId = 1 } });
            mockService.Setup(x => x.SearchUser(t))
                .Returns<UserModel>(r => new List<UserModel>() { new UserModel() { UserId = 2, FirstName = "User2", LastName = "kumar", EmployeeId = "Emp001", ProjectId = 1 } });

            controller.UserService = mockService.Object;
        }

        [PerfBenchmark(RunMode = RunMode.Iterations, TestMode = TestMode.Test, NumberOfIterations = 1)]
        [CounterThroughputAssertion("AddUserCounter", MustBe.GreaterThanOrEqualTo, AcceptableMinThroughput)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 600000)]
        public void AddUserTest(BenchmarkContext context)
        {
            addUserCounter = context.GetCounter(AddUserCounter);
            for (int i = 0; i < 500; i++)
            {
                userId++;
                UserModel t = new UserModel() { FirstName = "User" + userId, LastName = "kumar", EmployeeId = "Emp001", ProjectId = 1 };
                controller.AddUser(t);
                addUserCounter.Increment();
            }
        }

        [PerfBenchmark(RunMode = RunMode.Iterations, TestMode = TestMode.Test, NumberOfIterations = 1)]
        [CounterThroughputAssertion(UpdateUserCounter, MustBe.GreaterThanOrEqualTo, AcceptableMinThroughput)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 600000)]
        public void UpdateUserTest(BenchmarkContext context)
        {
            updateUserCounter = context.GetCounter(UpdateUserCounter);

            for (int i = 0; i < 500; i++)
            {
                userId++;
                UserModel t = new UserModel() { FirstName = "User" + userId, LastName = "kumar", EmployeeId = "Emp001", ProjectId = 1 };
                controller.UpdateUser(t);
                updateUserCounter.Increment();
            }
        }

        [PerfBenchmark(RunMode = RunMode.Iterations, TestMode = TestMode.Test, NumberOfIterations = 1)]
        [CounterThroughputAssertion(SearchUserCounter, MustBe.GreaterThanOrEqualTo, AcceptableMinThroughput)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 600000)]
        public void SearchUserTest(BenchmarkContext context)
        {
            searchUserCounter = context.GetCounter(SearchUserCounter);

            for (int i = 0; i < 500; i++)
            {
                userId++;
                UserModel t = new UserModel() { FirstName = "User" + userId, LastName = "kumar", EmployeeId = "Emp001", ProjectId = 1 };
                controller.Search(t);
                searchUserCounter.Increment();
            }
        }

        [PerfBenchmark(RunMode = RunMode.Iterations, TestMode = TestMode.Test, NumberOfIterations = 1)]
        [CounterThroughputAssertion(SearchUserCounter, MustBe.GreaterThanOrEqualTo, AcceptableMinThroughput)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 600000)]
        public void DeleteUserTest(BenchmarkContext context)
        {
            searchUserCounter = context.GetCounter(SearchUserCounter);

            for (int i = 0; i < 500; i++)
            {
                userId++;
                UserModel t = new UserModel() { FirstName = "User" + userId, LastName = "kumar", EmployeeId = "Emp001", ProjectId = 1 };
                controller.DeleteUser(t);
                searchUserCounter.Increment();
            }
        }
    }
}