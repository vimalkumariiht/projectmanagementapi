﻿using BL.Contracts;
using BL.Model;
using Moq;
using NBench;
using ProjectManagerApi.Controllers;
using System;
using System.Collections.Generic;

namespace ProjectManagerTest.NBench
{
    public class ProjectTest
    {
        private int projectId = 0;
        ProjectController controller;
        private const string AddProjectCounter = "AddProjectCounter";
        private const string UpdateProjectCounter = "UpdateProjectCounter";
        private const string SearchProjectCounter = "SearchProjectCounter";
        private const int AcceptableMinThroughput = 100;
        private Counter addProjectCounter;
        private Counter updateProjectCounter;
        private Counter searchProjectCounter;
        DateTime dtStart = DateTime.Today;
        DateTime dtEnd = DateTime.Today.AddDays(2);

        [PerfSetup]
        public void Setup()
        {
            controller = new ProjectController();
            var mockService = new Mock<IProjectOperation>();
            ProjectModel t = new ProjectModel() { ProjectName = "Project1", Priority = 1, StartDate = dtStart, EndDate = dtEnd };
            mockService.Setup(x => x.AddProject(t))
                    .Returns<ProjectModel>(r => new ProjectModel() { ProjectId = 2, ProjectName = "Project2", Priority = 1, StartDate = dtStart, EndDate = dtEnd });
            mockService.Setup(y => y.GetProject())
                .Returns(new List<ProjectModel>() { new ProjectModel() { ProjectId = 2, ProjectName = "Project2", Priority = 1, StartDate = dtStart, EndDate = dtEnd } });
            mockService.Setup(x => x.SearchProject("Project2"))
                .Returns<ProjectModel>(r => new List<ProjectModel>() { new ProjectModel() { ProjectId = 2, ProjectName = "Project2", Priority = 1, StartDate = dtStart, EndDate = dtEnd } });

            controller.ProjectService = mockService.Object;
        }

        [PerfBenchmark(RunMode = RunMode.Iterations, TestMode = TestMode.Test, NumberOfIterations = 1)]
        [CounterThroughputAssertion("AddProjectCounter", MustBe.GreaterThanOrEqualTo, AcceptableMinThroughput)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 600000)]
        public void AddProjectTest(BenchmarkContext context)
        {
            addProjectCounter = context.GetCounter(AddProjectCounter);
            for (int i = 0; i < 500; i++)
            {
                projectId++;
                ProjectModel t = new ProjectModel() { ProjectName = "Project" + projectId, Priority = 1, StartDate = dtStart, EndDate = dtEnd };
                controller.AddProject(t);
                addProjectCounter.Increment();
            }
        }

        [PerfBenchmark(RunMode = RunMode.Iterations, TestMode = TestMode.Test, NumberOfIterations = 1)]
        [CounterThroughputAssertion(UpdateProjectCounter, MustBe.GreaterThanOrEqualTo, AcceptableMinThroughput)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 600000)]
        public void UpdateProjectTest(BenchmarkContext context)
        {
            updateProjectCounter = context.GetCounter(UpdateProjectCounter);

            for (int i = 0; i < 500; i++)
            {
                projectId++;
                ProjectModel t = new ProjectModel() { ProjectName = "Project" + projectId, Priority = 1, StartDate = dtStart, EndDate = dtEnd };
                controller.UpdateProject(t);
                updateProjectCounter.Increment();
            }
        }

        [PerfBenchmark(RunMode = RunMode.Iterations, TestMode = TestMode.Test, NumberOfIterations = 1)]
        [CounterThroughputAssertion(SearchProjectCounter, MustBe.GreaterThanOrEqualTo, AcceptableMinThroughput)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 600000)]
        public void SearchProjectTest(BenchmarkContext context)
        {
            searchProjectCounter = context.GetCounter(SearchProjectCounter);

            for (int i = 0; i < 500; i++)
            {
                projectId++;
                ProjectModel t = new ProjectModel() { ProjectName = "Project" + projectId, Priority = 1, StartDate = dtStart, EndDate = dtEnd };
                controller.Search(t);
                searchProjectCounter.Increment();
            }
        }
    }
}
