﻿using BL.Contracts;
using BL.Model;
using Moq;
using NBench;
using ProjectManagerApi.Controllers;
using System;
using System.Collections.Generic;

namespace ProjectManagerTest.NBench
{
    public class TaskTest
    {
        private int taskId = 0;
        TaskController controller;
        private const string AddTaskCounter = "AddTaskCounter";
        private const string UpdateTaskCounter = "UpdateTaskCounter";
        private const string SearchTaskCounter = "SearchTaskCounter";
        private const int AcceptableMinThroughput = 100;
        private Counter addTaskCounter;
        private Counter updateTaskCounter;
        private Counter searchTaskCounter;
        DateTime dtStart = DateTime.Today;
        DateTime dtEnd = DateTime.Today.AddDays(2);

        [PerfSetup]
        public void Setup()
        {
            controller = new TaskController();
            DateTime dtStart = DateTime.Today;
            DateTime dtEnd = DateTime.Today.AddDays(2);
            var mockService = new Mock<ITaskOperation>();
            TaskModel t = new TaskModel() { TaskDescription = "Task1", Priority = 1, StartDate = dtStart, EndDate = dtEnd };
            mockService.Setup(x => x.AddTask(t))
                    .Returns<TaskModel>(r => new TaskModel() { TaskId = 2, TaskDescription = "Task2", Priority = 1, StartDate = dtStart, EndDate = dtEnd });
            mockService.Setup(y => y.GetTask())
                .Returns(new List<TaskModel>() { new TaskModel() { TaskId = 2, TaskDescription = "Task2", Priority = 1, StartDate = dtStart, EndDate = dtEnd } });
            mockService.Setup(x => x.SearchTask(t))
                .Returns<TaskModel>(r => new List<TaskModel>() { new TaskModel() { TaskId = 2, TaskDescription = "Task2", Priority = 1, StartDate = dtStart, EndDate = dtEnd } });

            controller.TaskService = mockService.Object;
        }

        [PerfBenchmark(RunMode = RunMode.Iterations, TestMode = TestMode.Test, NumberOfIterations = 1)]
        [CounterThroughputAssertion("AddTaskCounter", MustBe.GreaterThanOrEqualTo, AcceptableMinThroughput)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 600000)]
        public void AddTaskTest(BenchmarkContext context)
        {
            addTaskCounter = context.GetCounter(AddTaskCounter);
            for (int i = 0; i < 500; i++)
            {
                taskId++;
                TaskModel t = new TaskModel() { TaskDescription = "Task" + taskId, Priority = 1, StartDate = dtStart, EndDate = dtEnd };
                controller.AddTask(t);
                addTaskCounter.Increment();
            }
        }

        [PerfBenchmark(RunMode = RunMode.Iterations, TestMode = TestMode.Test, NumberOfIterations = 1)]
        [CounterThroughputAssertion(UpdateTaskCounter, MustBe.GreaterThanOrEqualTo, AcceptableMinThroughput)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 600000)]
        public void UpdateTaskTest(BenchmarkContext context)
        {
            updateTaskCounter = context.GetCounter(UpdateTaskCounter);

            for (int i = 0; i < 500; i++)
            {
                taskId++;
                TaskModel t = new TaskModel() { TaskDescription = "Task" + taskId, Priority = 1, StartDate = dtStart, EndDate = dtEnd };
                controller.UpdateTask(t);
                updateTaskCounter.Increment();
            }
        }

        [PerfBenchmark(RunMode = RunMode.Iterations, TestMode = TestMode.Test, NumberOfIterations = 1)]
        [CounterThroughputAssertion(SearchTaskCounter, MustBe.GreaterThanOrEqualTo, AcceptableMinThroughput)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 600000)]
        public void SearchTaskTest(BenchmarkContext context)
        {
            searchTaskCounter = context.GetCounter(SearchTaskCounter);

            for (int i = 0; i < 500; i++)
            {
                taskId++;
                TaskModel t = new TaskModel() { TaskDescription = "Task" + taskId, Priority = 1, StartDate = dtStart, EndDate = dtEnd };
                controller.Search(t);
                searchTaskCounter.Increment();
            }
        }
    }
}
