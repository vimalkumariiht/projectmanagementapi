﻿using BL.Contracts;
using BL.Model;
using Moq;
using NUnit.Framework;
using ProjectManagerApi.Controllers;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Results;

namespace ProjectManagerTest.NUnit
{
    [TestFixture]
    public class WebApiProjectTest
    {
        ProjectController controller;
        DateTime dtStart = DateTime.Today;
        DateTime dtEnd = DateTime.Today.AddDays(2);

        public WebApiProjectTest()
        {
            controller = new ProjectController();
        }
         
        [Test]
        public void AddProjectTest()
        {
            ProjectModel t = new ProjectModel() { ProjectName = "Project1", Priority = 1, StartDate = dtStart, EndDate = dtEnd };
            var mockService = new Mock<IProjectOperation>();
            mockService.Setup(x => x.AddProject(t))
                .Returns<ProjectModel>(r => new ProjectModel() { ProjectId = 1, ProjectName = "Project1", Priority = 1, StartDate = dtStart, EndDate = dtEnd });
 
            controller.ProjectService = mockService.Object;
             
            IHttpActionResult httpresult = controller.AddProject(t);
            var result = httpresult as OkNegotiatedContentResult<ProjectModel>;
            Assert.IsTrue(result.Content.ProjectName == "Project1");
            Assert.IsTrue(result.Content.Priority == 1);
            Assert.IsTrue(result.Content.StartDate == dtStart);
            Assert.IsTrue(result.Content.EndDate == dtEnd);
            Assert.IsTrue(result.Content.ProjectId > 0);
        }

        [Test]
        public void SearchProjectTestPositive()
        {
            ProjectModel t = new ProjectModel() { ProjectName = "Project2", Priority = 1, StartDate = dtStart, EndDate = dtEnd };
            var mockService = new Mock<IProjectOperation>();
            mockService.Setup(x => x.AddProject(t))
                .Returns<ProjectModel>(r => new ProjectModel() { ProjectName = "Project2", Priority = 1, StartDate = dtStart, EndDate = dtEnd });
            mockService.Setup(x => x.SearchProject("Project"))
                .Returns<ProjectModel>(r => new List<ProjectModel>() { new ProjectModel() { ProjectName = "Project2", Priority = 1, StartDate = dtStart, EndDate = dtEnd } });
             
            controller.ProjectService = mockService.Object;

            IHttpActionResult httpAddresult = controller.AddProject(t);

            IHttpActionResult httpSearchresult = controller.Search(t);
            var result = httpSearchresult as OkNegotiatedContentResult<List<ProjectModel>>;
            Assert.IsTrue(result.Content.Count >= 1);
        }

        [Test]
        public void SearchProjectTestNegative()
        {
            ProjectModel t = new ProjectModel() { ProjectName = "Project3", Priority = 1, StartDate = dtStart, EndDate = dtEnd };
            var mockService = new Mock<IProjectOperation>();
            mockService.Setup(x => x.AddProject(t))
                .Returns<ProjectModel>(r => new ProjectModel() { ProjectName = "Project3", Priority = 1, StartDate = dtStart, EndDate = dtEnd });
            mockService.Setup(x => x.SearchProject("Project"))
                .Returns<ProjectModel>(r => new List<ProjectModel>() { });
            controller.ProjectService = mockService.Object;

            IHttpActionResult httpAddresult = controller.AddProject(t);

            t.ProjectName = "Project100";

            IHttpActionResult httpSearchresult = controller.Search(t);
            var result = httpSearchresult as OkNegotiatedContentResult<List<ProjectModel>>;
            Assert.IsTrue(result.Content.Count == 0);
        }

        [Test]
        public void UpdateProjectTestPositive()
        {
            ProjectModel t = new ProjectModel() { ProjectName = "Project4", Priority = 1, StartDate = dtStart, EndDate = dtEnd };
            ProjectModel projectUpdated = new ProjectModel() { ProjectId = 4, ProjectName = "Project4", Priority = 1, StartDate = dtStart, EndDate = dtEnd };
            var mockService = new Mock<IProjectOperation>();
            mockService.Setup(x => x.AddProject(t))
                .Returns<ProjectModel>(r => new ProjectModel() { ProjectId = 4, ProjectName = "Project4", Priority = 1, StartDate = dtStart, EndDate = dtEnd });
            mockService.Setup(x => x.UpdateProject(projectUpdated))
                .Returns<ProjectModel>(r => new ProjectModel() { ProjectId = 4, ProjectName = "Project101", Priority = 1, StartDate = dtStart, EndDate = dtEnd });
            controller.ProjectService = mockService.Object;

            IHttpActionResult httpAddresult = controller.AddProject(t);
            var addResult = httpAddresult as OkNegotiatedContentResult<ProjectModel>;
            addResult.Content.ProjectName = "Project101";

            IHttpActionResult httpSearchresult = controller.UpdateProject(projectUpdated);
            var result = httpSearchresult as OkNegotiatedContentResult<ProjectModel>;
            Assert.IsTrue(result.Content != null);
            Assert.IsTrue(result.Content.ProjectName == "Project101");
        }

        [Test]
        public void UpdateProjectTestNegative()
        {
            ProjectModel t = new ProjectModel() { ProjectName = "Project5", Priority = 1, StartDate = dtStart, EndDate = dtEnd };
            ProjectModel projecttoUpdate = new ProjectModel() { ProjectId = 5, ProjectName = "Project5", Priority = 1, StartDate = dtStart, EndDate = dtEnd };
            var mockService = new Mock<IProjectOperation>();
            mockService.Setup(x => x.AddProject(t))
                .Returns<ProjectModel>(r => new ProjectModel() { ProjectId = 5, ProjectName = "Project5", Priority = 1, StartDate = dtStart, EndDate = dtEnd });
            mockService.Setup(x => x.UpdateProject(projecttoUpdate))
                .Returns<ProjectModel>(r => new ProjectModel() { ProjectId = 5, ProjectName = "Project102", Priority = 1, StartDate = dtStart, EndDate = dtEnd });
            controller.ProjectService = mockService.Object;

            IHttpActionResult httpAddresult = controller.AddProject(t);

            IHttpActionResult httpSearchresult = controller.UpdateProject(projecttoUpdate);
            var result = httpSearchresult as OkNegotiatedContentResult<ProjectModel>;

            Assert.IsFalse(result.Content.ProjectName == t.ProjectName);
        }

        [Test]
        public void GetProjectTestPositive()
        {
            ProjectModel t = new ProjectModel() { ProjectName = "Project2", Priority = 1, StartDate = dtStart, EndDate = dtEnd };
            var mockService = new Mock<IProjectOperation>();
            mockService.Setup(x => x.AddProject(t))
                .Returns<ProjectModel>(r => new ProjectModel() { ProjectId = 2, ProjectName = "Project2", Priority = 1, StartDate = dtStart, EndDate = dtEnd });
            mockService.Setup(y => y.GetProject())
                .Returns(new List<ProjectModel>() { new ProjectModel() { ProjectId = 2, ProjectName = "Task2", Priority = 1, StartDate = dtStart, EndDate = dtEnd } });

            ProjectController controller = new ProjectController();
            controller.ProjectService = mockService.Object;

            IHttpActionResult httpAddresult = controller.AddProject(t);

            IHttpActionResult httpSearchresult = controller.GetProject();
            var result = httpSearchresult as OkNegotiatedContentResult<List<ProjectModel>>;
            Assert.IsTrue(result.Content.Count >= 1);
        }
    }
}
