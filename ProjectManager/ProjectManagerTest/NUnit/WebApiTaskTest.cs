﻿using BL.Contracts;
using BL.Model;
using Moq;
using NUnit.Framework;
using ProjectManagerApi.Controllers;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Results;

namespace ProjectManagerTest.NUnit
{
    [TestFixture]
    public class WebApiTaskTest
    {
        TaskController controller;
        DateTime dtStart = DateTime.Today;
        DateTime dtEnd = DateTime.Today.AddDays(2);

        public WebApiTaskTest()
        {
            controller = new TaskController();
        }
        [Test]
        public void AddTaskTest()
        {
            TaskModel t = new TaskModel() { TaskDescription = "Task1", Priority = 1, StartDate = dtStart, EndDate = dtEnd };
            var mockService = new Mock<ITaskOperation>();
            mockService.Setup(x => x.AddTask(t))
                .Returns<TaskModel>(r => new TaskModel() { TaskId = 1, TaskDescription = "Task1", Priority = 1, StartDate = dtStart, EndDate = dtEnd });
            
            controller.TaskService = mockService.Object;
            
            IHttpActionResult httpresult = controller.AddTask(t);
            var result = httpresult as OkNegotiatedContentResult<TaskModel>;
            Assert.IsTrue(result.Content.TaskDescription == "Task1");
            Assert.IsTrue(result.Content.Priority == 1);
            Assert.IsTrue(result.Content.StartDate == dtStart);
            Assert.IsTrue(result.Content.EndDate == dtEnd);
            Assert.IsTrue(result.Content.TaskId > 0);
        }

        [Test]
        public void SearchTaskTestPositive()
        {
            TaskModel t = new TaskModel() { TaskDescription = "Task2", Priority = 1, StartDate = dtStart, EndDate = dtEnd };
            var mockService = new Mock<ITaskOperation>();
            mockService.Setup(x => x.AddTask(t))
                .Returns<TaskModel>(r => new TaskModel() { TaskId = 2, TaskDescription = "Task2", Priority = 1, StartDate = dtStart, EndDate = dtEnd });
            mockService.Setup(x => x.SearchTask(t))
                .Returns<TaskModel>(r => new List<TaskModel>() { new TaskModel() { TaskId = 2, TaskDescription = "Task2", Priority = 1, StartDate = dtStart, EndDate = dtEnd } });
            
            controller.TaskService = mockService.Object;

            IHttpActionResult httpAddresult = controller.AddTask(t);

            IHttpActionResult httpSearchresult = controller.Search(t);
            var result = httpSearchresult as OkNegotiatedContentResult<List<TaskModel>>;
            Assert.IsTrue(result.Content.Count >= 1);
        }
         
        [Test]
        public void SearchTaskTestNegative()
        {
            TaskModel t = new TaskModel() { TaskDescription = "Task3", Priority = 1, StartDate = dtStart, EndDate = dtEnd };
            var mockService = new Mock<ITaskOperation>();
            mockService.Setup(x => x.AddTask(t))
                .Returns<TaskModel>(r => new TaskModel() { TaskId = 2, TaskDescription = "Task2", Priority = 1, StartDate = dtStart, EndDate = dtEnd });
            mockService.Setup(x => x.SearchTask(t))
                .Returns<TaskModel>(r => new List<TaskModel>() { });
            controller.TaskService = mockService.Object;

            IHttpActionResult httpAddresult = controller.AddTask(t);

            t.TaskDescription = "Task100";

            IHttpActionResult httpSearchresult = controller.Search(t);
            var result = httpSearchresult as OkNegotiatedContentResult<List<TaskModel>>;
            Assert.IsTrue(result.Content.Count == 0);
        }

        [Test]
        public void UpdateTaskTestPositive()
        {
            TaskModel t = new TaskModel() { TaskDescription = "Task4", Priority = 1, StartDate = dtStart, EndDate = dtEnd };
            TaskModel taskUpdated = new TaskModel() { TaskId = 2, TaskDescription = "Task4", Priority = 1, StartDate = dtStart, EndDate = dtEnd };
            var mockService = new Mock<ITaskOperation>();
            mockService.Setup(x => x.AddTask(t))
                .Returns<TaskModel>(r => new TaskModel() { TaskId = 2, TaskDescription = "Task4", Priority = 1, StartDate = dtStart, EndDate = dtEnd });
            mockService.Setup(x => x.UpdateTask(taskUpdated))
                .Returns<TaskModel>(r => new TaskModel() { TaskId = 2, TaskDescription = "Task101", Priority = 1, StartDate = dtStart, EndDate = dtEnd });
            controller.TaskService = mockService.Object;

            IHttpActionResult httpAddresult = controller.AddTask(t);
            var addResult = httpAddresult as OkNegotiatedContentResult<TaskModel>;
            addResult.Content.TaskDescription = "Task101";

            IHttpActionResult httpSearchresult = controller.UpdateTask(taskUpdated);
            var result = httpSearchresult as OkNegotiatedContentResult<TaskModel>;
            Assert.IsTrue(result.Content != null);
            Assert.IsTrue(result.Content.TaskDescription == "Task101");
        }

        [Test]
        public void UpdateTaskTestNegative()
        {
            TaskModel t = new TaskModel() { TaskDescription = "Task5", Priority = 1, StartDate = dtStart, EndDate = dtEnd };
            TaskModel tasktoUpdate = new TaskModel() { TaskId = 2, TaskDescription = "Task4", Priority = 1, StartDate = dtStart, EndDate = dtEnd };
            var mockService = new Mock<ITaskOperation>();
            mockService.Setup(x => x.AddTask(t))
                .Returns<TaskModel>(r => new TaskModel() { TaskId = 2, TaskDescription = "Task4", Priority = 1, StartDate = dtStart, EndDate = dtEnd });
            mockService.Setup(x => x.UpdateTask(tasktoUpdate))
                .Returns<TaskModel>(r => new TaskModel() { TaskId = 2, TaskDescription = "Task102", Priority = 1, StartDate = dtStart, EndDate = dtEnd });
            controller.TaskService = mockService.Object;

            IHttpActionResult httpAddresult = controller.AddTask(t);

            IHttpActionResult httpSearchresult = controller.UpdateTask(tasktoUpdate);
            var result = httpSearchresult as OkNegotiatedContentResult<TaskModel>;

            Assert.IsFalse(result.Content.TaskDescription == t.TaskDescription);
        }

        [Test]
        public void GetTaskTestPositive()
        {
            TaskModel t = new TaskModel() { TaskDescription = "Task2", Priority = 1, StartDate = dtStart, EndDate = dtEnd };
            var mockService = new Mock<ITaskOperation>();
            mockService.Setup(x => x.AddTask(t))
                .Returns<TaskModel>(r => new TaskModel() { TaskId = 2, TaskDescription = "Task2", Priority = 1, StartDate = dtStart, EndDate = dtEnd });
            mockService.Setup(y => y.GetTask())
                .Returns(new List<TaskModel>() { new TaskModel() { TaskId = 2, TaskDescription = "Task2", Priority = 1, StartDate = dtStart, EndDate = dtEnd } });

            controller.TaskService = mockService.Object;

            IHttpActionResult httpAddresult = controller.AddTask(t);

            IHttpActionResult httpSearchresult = controller.GetTask();
            var result = httpSearchresult as OkNegotiatedContentResult<List<TaskModel>>;
            Assert.IsTrue(result.Content.Count >= 1);
        }
    }
}