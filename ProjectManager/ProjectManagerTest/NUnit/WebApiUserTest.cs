﻿using BL.Contracts;
using BL.Model;
using Moq;
using NUnit.Framework;
using ProjectManagerApi.Controllers;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Results;

namespace ProjectManagerTest.NUnit
{
    [TestFixture]
    public class WebApiUserTest
    {
        UserController controller;
         
        public WebApiUserTest()
        {
            controller = new UserController();
        }
        [Test]
        public void AddUserTest()
        {
           UserModel t = new UserModel() { FirstName = "User1", LastName = "kumar", EmployeeId = "Emp001", ProjectId = 1 };
            var mockService = new Mock<IUserOperation>();
            mockService.Setup(x => x.AddUser(t))
                .Returns<UserModel>(r => new UserModel() { UserId = 1, FirstName = "User1", LastName = "kumar", EmployeeId = "Emp001", ProjectId = 1 });
             
            controller.UserService = mockService.Object;
             
            IHttpActionResult httpresult = controller.AddUser(t);
            var result = httpresult as OkNegotiatedContentResult<UserModel>;
            Assert.IsTrue(result.Content.FirstName == "User1");
            Assert.IsTrue(result.Content.LastName == "kumar");
            Assert.IsTrue(result.Content.EmployeeId == "Emp001");
            Assert.IsTrue(result.Content.ProjectId == 1);
            Assert.IsTrue(result.Content.UserId > 0);
        }

        [Test]
        public void SearchUserTestPositive()
        {
           UserModel t = new UserModel() { FirstName = "User2", LastName = "kumar", EmployeeId = "Emp001", ProjectId = 1 };
            var mockService = new Mock<IUserOperation>();
            mockService.Setup(x => x.AddUser(t))
                .Returns<UserModel>(r => new UserModel() { FirstName = "User2", LastName = "kumar", EmployeeId = "Emp001", ProjectId = 1 });
            mockService.Setup(x => x.SearchUser(t))
                .Returns<UserModel>(r => new List<UserModel>() { new UserModel() { FirstName = "User2", LastName = "kumar", EmployeeId = "Emp001", ProjectId = 1 } });
            
            controller.UserService = mockService.Object;

            IHttpActionResult httpAddresult = controller.AddUser(t);

            IHttpActionResult httpSearchresult = controller.Search(t);
            var result = httpSearchresult as OkNegotiatedContentResult<List<UserModel>>;
            Assert.IsTrue(result.Content.Count >= 1);
        }

        [Test]
        public void SearchUserTestNegative()
        {
           UserModel t = new UserModel() { FirstName = "User3", LastName = "kumar", EmployeeId = "Emp001", ProjectId = 1 };
            var mockService = new Mock<IUserOperation>();
            mockService.Setup(x => x.AddUser(t))
                .Returns<UserModel>(r => new UserModel() { FirstName = "User3", LastName = "kumar", EmployeeId = "Emp001", ProjectId = 1 });
            mockService.Setup(x => x.SearchUser(t))
                .Returns<UserModel>(r => new List<UserModel>() { });
            controller.UserService = mockService.Object;

            IHttpActionResult httpAddresult = controller.AddUser(t);

            t.FirstName = "User100";

            IHttpActionResult httpSearchresult = controller.Search(t);
            var result = httpSearchresult as OkNegotiatedContentResult<List<UserModel>>;
            Assert.IsTrue(result.Content.Count == 0);
        }

        [Test]
        public void UpdateUserTestPositive()
        {
           UserModel t = new UserModel() { FirstName = "User4", LastName = "kumar", EmployeeId = "Emp001", ProjectId = 1 };
           UserModel projectUpdated = new UserModel() { UserId = 4, FirstName = "User4", LastName = "kumar", EmployeeId = "Emp001", ProjectId = 1 };
            var mockService = new Mock<IUserOperation>();
            mockService.Setup(x => x.AddUser(t))
                .Returns<UserModel>(r => new UserModel() { UserId = 4, FirstName = "User4", LastName = "kumar", EmployeeId = "Emp001", ProjectId = 1 });
            mockService.Setup(x => x.UpdateUser(projectUpdated))
                .Returns<UserModel>(r => new UserModel() { UserId = 4, FirstName = "User101", LastName = "kumar", EmployeeId = "Emp001", ProjectId = 1 });
            controller.UserService = mockService.Object;

            IHttpActionResult httpAddresult = controller.AddUser(t);
            var addResult = httpAddresult as OkNegotiatedContentResult<UserModel>;
            addResult.Content.FirstName = "User101";

            IHttpActionResult httpSearchresult = controller.UpdateUser(projectUpdated);
            var result = httpSearchresult as OkNegotiatedContentResult<UserModel>;
            Assert.IsTrue(result.Content != null);
            Assert.IsTrue(result.Content.FirstName == "User101");
        }

        [Test]
        public void UpdateUserTestNegative()
        {
           UserModel t = new UserModel() { FirstName = "User5", LastName = "kumar", EmployeeId = "Emp001", ProjectId = 1 };
           UserModel usertoUpdate = new UserModel() { UserId = 5, FirstName = "User5", LastName = "kumar", EmployeeId = "Emp001", ProjectId = 1 };
            var mockService = new Mock<IUserOperation>();
            mockService.Setup(x => x.AddUser(t))
                .Returns<UserModel>(r => new UserModel() { UserId = 5, FirstName = "User5", LastName = "kumar", EmployeeId = "Emp001", ProjectId = 1 });
            mockService.Setup(x => x.UpdateUser(usertoUpdate))
                .Returns<UserModel>(r => new UserModel() { UserId = 5, FirstName = "User102", LastName = "kumar", EmployeeId = "Emp001", ProjectId = 1 });
            controller.UserService = mockService.Object;

            IHttpActionResult httpAddresult = controller.AddUser(t);

            IHttpActionResult httpSearchresult = controller.UpdateUser(usertoUpdate);
            var result = httpSearchresult as OkNegotiatedContentResult<UserModel>;

            Assert.IsFalse(result.Content.FirstName == t.FirstName);
        }

        [Test]
        public void GetUserTestPositive()
        {
           UserModel t = new UserModel() { FirstName = "User2", LastName = "kumar", EmployeeId = "Emp001", ProjectId = 1 };
            var mockService = new Mock<IUserOperation>();
            mockService.Setup(x => x.AddUser(t))
                .Returns<UserModel>(r => new UserModel() { UserId = 2, FirstName = "User2", LastName = "kumar", EmployeeId = "Emp001", ProjectId = 1 });
            mockService.Setup(y => y.GetUser())
                .Returns(new List<UserModel>() { new UserModel() { UserId = 2, FirstName = "Task2", LastName = "kumar", EmployeeId = "Emp001", ProjectId = 1 } });
            
            controller.UserService = mockService.Object;

            IHttpActionResult httpAddresult = controller.AddUser(t);

            IHttpActionResult httpSearchresult = controller.GetUser();
            var result = httpSearchresult as OkNegotiatedContentResult<List<UserModel>>;
            Assert.IsTrue(result.Content.Count >= 1);
        }

        [Test]
        public void DeleteUserTestPositive()
        {
            UserModel user = new UserModel() { UserId = 6, FirstName = "User6", LastName = "kumar", EmployeeId = "Emp001", ProjectId = 1 };
            var mockService = new Mock<IUserOperation>();
            mockService.Setup(x => x.AddUser(user))
                .Returns<UserModel>(r => user);
            mockService.Setup(x => x.DeleteUser(user))
                .Returns<UserModel>(r => true);
            controller.UserService = mockService.Object;

            IHttpActionResult httpAddresult = controller.AddUser(user);
            var addResult = httpAddresult as OkNegotiatedContentResult<UserModel>;

            IHttpActionResult httpSearchresult = controller.DeleteUser(addResult.Content);
            var result = httpSearchresult as OkNegotiatedContentResult<bool>;
            Assert.IsTrue(result.Content);
        }

        [Test]
        public void DeleteUserTestNegative()
        {
            UserModel user = new UserModel() { UserId = 6, FirstName = "User6", LastName = "kumar", EmployeeId = "Emp001", ProjectId = 1 };
            var mockService = new Mock<IUserOperation>();
            mockService.Setup(x => x.AddUser(user))
                .Returns<UserModel>(r => user);
            UserModel userDeleted = new UserModel() { UserId = 6, FirstName = "User6", LastName = "kumar", EmployeeId = "Emp001", ProjectId = 1 };
            mockService.Setup(x => x.DeleteUser(userDeleted))
                .Returns<UserModel>(r => false);
            controller.UserService = mockService.Object;

            IHttpActionResult httpAddresult = controller.AddUser(user);
            var addResult = httpAddresult as OkNegotiatedContentResult<UserModel>;

            IHttpActionResult httpSearchresult = controller.DeleteUser(addResult.Content);
            var result = httpSearchresult as OkNegotiatedContentResult<bool>;
            Assert.IsFalse(result.Content);
        }
    }
}