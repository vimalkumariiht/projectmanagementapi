﻿using BL.Contracts;
using BL.Model;
using BL.Operation;
using System.Web.Http;
using Unity;

namespace ProjectManagerApi.Controllers
{
    [RoutePrefix("Task")]
    public class TaskController : ApiController
    {
        public ITaskOperation TaskService { get; set; }

        public TaskController()
        {
            TaskService = Context.Container.Resolve<TaskOperation>();
        }


        [Route("Get")]
        [HttpGet]
        public IHttpActionResult GetTask()
        {
            return Ok(TaskService.GetTask());
        }

        [Route("AddTask")]
        [HttpPost]
        public IHttpActionResult AddTask(TaskModel model)
        {
            return Ok(TaskService.AddTask(model));
        }

        [Route("UpdateTask")]
        [HttpPost]
        public IHttpActionResult UpdateTask(TaskModel model)
        {
            return Ok(TaskService.UpdateTask(model));
        }

        [Route("Search")]
        [HttpPost]
        public IHttpActionResult Search(TaskModel model)
        {
            return Ok(TaskService.SearchTask(model));
        }

    }
}
