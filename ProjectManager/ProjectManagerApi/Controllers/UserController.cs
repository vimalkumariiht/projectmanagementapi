﻿using BL.Contracts;
using BL.Model;
using BL.Operation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Unity;

namespace ProjectManagerApi.Controllers
{
    [RoutePrefix("User")]
    public class UserController : ApiController
    {
        public IUserOperation UserService { get; set; }

        public UserController()
        {
            UserService = Context.Container.Resolve<UserOperation>();
        }


        [Route("Get")]
        [HttpGet]
        public IHttpActionResult GetUser()
        {
            return Ok(UserService.GetUser());
        }

        [Route("AddUser")]
        [HttpPost]
        public IHttpActionResult AddUser(UserModel model)
        {
            return Ok(UserService.AddUser(model));
        }

        [Route("UpdateUser")]
        [HttpPost]
        public IHttpActionResult UpdateUser(UserModel model)
        {
            return Ok(UserService.UpdateUser(model));
        }

        [Route("Search")]
        [HttpPost]
        public IHttpActionResult Search(UserModel model)
        {
            return Ok(UserService.SearchUser(model));
        }

        [Route("DeleteUser")]
        [HttpPost]
        public IHttpActionResult DeleteUser(UserModel model)
        {
            return Ok(UserService.DeleteUser(model));
        }
    }
}