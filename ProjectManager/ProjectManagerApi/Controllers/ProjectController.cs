﻿using BL.Contracts;
using BL.Model;
using BL.Operation;
using System.Web.Http;
using Unity;

namespace ProjectManagerApi.Controllers
{
    [RoutePrefix("Project")]
    public class ProjectController : ApiController
    {
        public IProjectOperation ProjectService { get; set; }

        public ProjectController()
        {
            ProjectService = Context.Container.Resolve<ProjectOperation>();
        }

        [Route("Get")]
        [HttpGet]
        public IHttpActionResult GetProject()
        {
            return Ok(ProjectService.GetProject());
        }
         
        [Route("AddProject")]
        [HttpPost]
        public IHttpActionResult AddProject(ProjectModel model)
        {
            return Ok(ProjectService.AddProject(model));
        }

        [Route("UpdateProject")]
        [HttpPost]
        public IHttpActionResult UpdateProject(ProjectModel model)
        {
            return Ok(ProjectService.UpdateProject(model));
        }
        
        [Route("Search")]
        [HttpPost]
        public IHttpActionResult Search(ProjectModel model)
        {
            return Ok(ProjectService.SearchProject(model.SearchText));
        }
    }
}
