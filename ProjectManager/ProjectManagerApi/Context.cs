﻿using BL;
using BL.Contracts;
using BL.Operation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Unity;

namespace ProjectManagerApi
{
    public class Context
    {
        static Context()
        {
            Container = new UnityContainer();
            Container.RegisterType<ITaskOperation, TaskOperation>();
            Container.RegisterType<IProjectOperation, ProjectOperation>();
            Container.RegisterType<IUserOperation, UserOperation>();
        }

        public static IUnityContainer Container { get; set; }
    }
}