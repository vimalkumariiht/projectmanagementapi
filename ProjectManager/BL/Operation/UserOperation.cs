﻿using BL.Contracts;
using BL.Model;
using DL;
using System.Collections.Generic;
using System.Linq;

namespace BL.Operation
{
    public class UserOperation : IUserOperation
    {
        public List<UserModel> GetUser()
        {
            using (var ctx = new ProjectManagerContext())
            {
                return ctx.Users.ToList().Select(a => a.GetUserModel()).ToList();
            }
        }

        public UserModel GetUserById(int userId)
        {
            using (var ctx = new ProjectManagerContext())
            {
                return ctx.Users.FirstOrDefault(a => a.UserId == userId).GetUserModel();
            }
        }

        public List<UserModel> SearchUser(UserModel t)
        {
            using (var ctx = new ProjectManagerContext())
            {
                var query = ctx.Users.ToList();
                if (!string.IsNullOrEmpty(t.SearchText))
                {
                    query = ctx.Users.Where(a =>
                    (!string.IsNullOrEmpty(a.FirstName) && a.FirstName.Contains(t.SearchText)) ||
                    (!string.IsNullOrEmpty(a.LastName) && a.LastName.Contains(t.SearchText)) ||
                    (!string.IsNullOrEmpty(a.EmployeeId) && a.EmployeeId.Contains(t.SearchText)) ||
                    (a.UserId.ToString().Contains(t.SearchText))).ToList();
                }

                var entitys = query.ToList();
                return entitys.Select(a => a.GetUserModel()).ToList();
            }
        }

        public UserModel AddUser(UserModel userModel)
        {
            using (var ctx = new ProjectManagerContext())
            {
                var entity = userModel.GetUser();
                ctx.Entry(entity).State = System.Data.Entity.EntityState.Added;
                ctx.SaveChanges();
                return entity.GetUserModel();
            }
        }

        public UserModel UpdateUser(UserModel userModel)
        {
            using (var ctx = new ProjectManagerContext())
            {
                var entity = userModel.GetUser();
                ctx.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                ctx.SaveChanges();
                return entity.GetUserModel();
            }
        }

        public bool DeleteUser(UserModel userModel)
        {
            using (var ctx = new ProjectManagerContext())
            {
                var user = ctx.Users.FirstOrDefault(a => a.UserId == userModel.UserId);
                var projcount = ctx.Projects.Count(a => a.ManagerId == user.UserId);
                var taskcount = ctx.Tasks.Count(a => a.UserId == user.UserId);
                if (projcount > 0 || taskcount > 0) return false;
                if (user != null)
                {
                    ctx.Entry(user).State = System.Data.Entity.EntityState.Deleted;
                    ctx.SaveChanges();
                    return true;
                }
                return false;
            }
        }
    }
}