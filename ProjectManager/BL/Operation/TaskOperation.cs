﻿using BL.Contracts;
using BL.Model;
using DL;
using System.Collections.Generic;
using System.Linq;

namespace BL.Operation
{
    public class TaskOperation : ITaskOperation
    {
        public List<TaskModel> GetTask()
        {
            using (var ctx = new ProjectManagerContext())
            {
                return ctx.Tasks.ToList().Select(a => a.GetTaskModel()).ToList();
            }
        }

        public TaskModel GetTaskById(int taskId)
        {
            using (var ctx = new ProjectManagerContext())
            {
                return ctx.Tasks.FirstOrDefault(a => a.TaskId == taskId).GetTaskModel();
            }
        }

        public List<TaskModel> SearchTask(TaskModel t)
        {
            using (var ctx = new ProjectManagerContext())
            {
                var query = ctx.Tasks.ToList();
                if (!string.IsNullOrEmpty(t.SearchText))
                {
                    query = query.Where(a =>
                        (a.TaskDescription.Contains(t.SearchText)) ||
                        (a.ParentId.ToString().Contains(t.SearchText)) ||
                        (a.StartDate != null && a.StartDate.Value.ToString("MM/dd/yyyy").Contains(t.SearchText)) ||
                        (a.EndDate != null && a.StartDate.Value.ToString("MM/dd/yyyy").Contains(t.SearchText))).ToList();
                }
                
                return query.Select(a => a.GetTaskModel()).ToList();
            }
        }

        public TaskModel AddTask(TaskModel taskModel)
        {
            using (var ctx = new ProjectManagerContext())
            {
                var entity = taskModel.GetTask();
                ctx.Entry(entity).State = System.Data.Entity.EntityState.Added;
                ctx.SaveChanges();
                return entity.GetTaskModel();
            }
        }

        public TaskModel UpdateTask(TaskModel taskModel)
        {
            using (var ctx = new ProjectManagerContext())
            {
                var entity = taskModel.GetTask();
                ctx.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                ctx.SaveChanges();
                return entity.GetTaskModel();
            }
        }

    }
}