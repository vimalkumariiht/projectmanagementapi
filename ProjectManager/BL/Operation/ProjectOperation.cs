﻿using BL.Contracts;
using BL.Model;
using DL;
using System.Collections.Generic;
using System.Linq;

namespace BL.Operation
{
    public class ProjectOperation : IProjectOperation
    {
        public List<ProjectModel> GetProject()
        {
            using (var ctx = new ProjectManagerContext())
            {
                return ctx.Projects.ToList().Select(a => a.GetProjectModel()).ToList();
            }
        }

        public ProjectModel GetProjectById(int projectId)
        {
            using (var ctx = new ProjectManagerContext())
            {
                return ctx.Projects.FirstOrDefault(a => a.ProjectId == projectId).GetProjectModel();
            }
        }

        public List<ProjectModel> SearchProject(string searchText)
        {
            using (var ctx = new ProjectManagerContext())
            {
                var query = ctx.Projects.Include("Tasks").ToList();
                if (!string.IsNullOrEmpty(searchText))
                {
                    query = query.Where(a => a.ProjectName.Contains(searchText) ||
                    (a.StartDate != null && a.StartDate.Value.ToString("MM/dd/yyyy").Contains(searchText)) ||
                    (a.EndDate != null && a.EndDate.Value.ToString("MM/dd/yyyy").ToString().Contains(searchText)) ||
                    (a.Priority != null && a.Priority.ToString() == searchText)).ToList();
                }  
                return query.Select(a => a.GetProjectModel()).ToList();
            }
        }

        public ProjectModel AddProject(ProjectModel projectModel)
        {
            using (var ctx = new ProjectManagerContext())
            {
                var entity = projectModel.GetProject();
                ctx.Entry(entity).State = System.Data.Entity.EntityState.Added;
                ctx.SaveChanges();
                return entity.GetProjectModel();
            }
        }

        public ProjectModel UpdateProject(ProjectModel projectModel)
        {
            using (var ctx = new ProjectManagerContext())
            {
                var entity = projectModel.GetProject();
                ctx.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                ctx.SaveChanges();
                return entity.GetProjectModel();
            }
        }

    }
}
