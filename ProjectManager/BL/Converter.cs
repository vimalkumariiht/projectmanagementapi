﻿using BL.Model;
using DL;
using System;

namespace BL
{
    public static class Converter
    {
        public static Task GetTask(this TaskModel model)
        {
            return new Task
            {
                TaskId = model.TaskId,
                TaskDescription = model.TaskDescription,
                ParentId = model.ParentId,
                StartDate = model.StartDate,
                EndDate = model.EndDate,
                Priority = model.Priority,
                ProjectId = model.ProjectId,
                UserId = model.UserId
            };
        }

        public static TaskModel GetTaskModel(this Task model)
        {
            return new TaskModel
            {
                TaskId = model.TaskId,
                TaskDescription = model.TaskDescription,
                ParentId = model.ParentId,
                StartDate = model.StartDate,
                EndDate = model.EndDate,
                Priority = model.Priority,
                Completed = model.EndDate <= DateTime.Today,
                ProjectId = model.ProjectId,
                UserId = model.UserId
            };
        }

        public static User GetUser(this UserModel model)
        {
            return new User
            {
                EmployeeId = model.EmployeeId,
                FirstName = model.FirstName,
                LastName = model.LastName,
                ProjectId = model.ProjectId,
                UserId = model.UserId,
            };
        }

        public static UserModel GetUserModel(this User model)
        {
            return new UserModel
            {
                EmployeeId = model.EmployeeId,
                FirstName = model.FirstName,
                LastName = model.LastName,
                ProjectId = model.ProjectId,
                UserId = model.UserId,
            };
        }

        public static Project GetProject(this ProjectModel model)
        {
            return new Project
            {
                ProjectId = model.ProjectId,
                ProjectName = model.ProjectName,
                StartDate = model.StartDate,
                EndDate = model.EndDate,
                Priority = model.Priority,
                ManagerId = model.ManagerId
            };
        }

        public static ProjectModel GetProjectModel(this Project model)
        {
            return new ProjectModel
            {
                ProjectId = model.ProjectId,
                ProjectName = model.ProjectName,
                StartDate = model.StartDate,
                EndDate = model.EndDate,
                Priority = model.Priority,
                ManagerId = model.ManagerId,
                NoOfTasks = model.Tasks != null ? model.Tasks.Count : 0,
                Completed = model.EndDate <= DateTime.Today
            };
        }
    }
}