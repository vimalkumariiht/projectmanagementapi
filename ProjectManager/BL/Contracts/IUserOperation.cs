﻿using BL.Model;
using System.Collections.Generic;

namespace BL.Contracts
{
    public interface IUserOperation
    {
        List<UserModel> GetUser();
        UserModel GetUserById(int userId);
        List<UserModel> SearchUser(UserModel userModel);
        UserModel AddUser(UserModel userModel);
        UserModel UpdateUser(UserModel userModel);
        bool DeleteUser(UserModel userModel);
    }
}