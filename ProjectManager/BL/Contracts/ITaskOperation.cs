﻿using BL.Model;
using System.Collections.Generic;

namespace BL.Contracts
{
    public interface ITaskOperation
    {
        List<TaskModel> GetTask();
        TaskModel GetTaskById(int taskId);
        List<TaskModel> SearchTask(TaskModel taskModel);
        TaskModel AddTask(TaskModel taskModel);
        TaskModel UpdateTask(TaskModel taskModel);
    }
}
