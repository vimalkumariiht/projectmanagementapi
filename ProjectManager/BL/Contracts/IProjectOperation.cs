﻿using BL.Model;
using System.Collections.Generic;

namespace BL.Contracts
{
    public interface IProjectOperation
    {
        List<ProjectModel> GetProject();
        ProjectModel GetProjectById(int projectId);
        List<ProjectModel> SearchProject(string searchText);
        ProjectModel AddProject(ProjectModel projectModel);
        ProjectModel UpdateProject(ProjectModel projectModel);
    }
}
