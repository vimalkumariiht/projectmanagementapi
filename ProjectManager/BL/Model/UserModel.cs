﻿using System.Collections.Generic;

namespace BL.Model
{
    public class UserModel
    { 
        public int UserId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string EmployeeId { get; set; }

        public int? ProjectId { get; set; }

        public string SearchText { get; set; }

        public ProjectModel Project { get; set; }

        public List<TaskModel> Tasks { get; set; }
    }
}