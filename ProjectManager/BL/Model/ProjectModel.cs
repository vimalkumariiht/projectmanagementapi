﻿using System;
using System.Collections.Generic;

namespace BL.Model
{
    public class ProjectModel
    {
        public int ProjectId { get; set; }
         
        public string ProjectName { get; set; }
        
        public DateTime? StartDate { get; set; }
         
        public DateTime? EndDate { get; set; }
         
        public int? Priority { get; set; }

        public int? ManagerId { get; set; }

        public int? NoOfTasks { get; set; }

        public bool Completed { get; set; }

        public string SearchText { get; set; }
        
        public List<TaskModel> Tasks { get; set; }

        public List<UserModel> Users { get; set; }
    }
}
