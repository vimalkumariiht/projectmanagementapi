﻿using System;

namespace BL.Model
{
    public class TaskModel
    {
        public int TaskId { get; set; }
        public int? ParentId { get; set; }
        public string TaskDescription { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? Priority { get; set; }
        public bool Completed { get; set; }
        public int? ProjectId { get; set; }
        public int? UserId { get; set; }

        public string SearchText { get; set; }
    }
}
