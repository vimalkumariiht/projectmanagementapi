﻿# ProjectManagerTest.NBench.TaskTest+NBenchTest+AddTaskTest
_12/21/2018 8:17:10 PM_
### System Info
```ini
NBench=NBench, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
OS=Microsoft Windows NT 6.2.9200.0
ProcessorCount=4
CLR=4.0.30319.42000,IsMono=False,MaxGcGeneration=2
```

### NBench Settings
```ini
RunMode=Iterations, TestMode=Test
NumberOfIterations=1, MaximumRunTime=00:00:01
Concurrent=False
Tracing=False
```

## Data
-------------------

### Totals
|          Metric |           Units |             Max |         Average |             Min |          StdDev |
|---------------- |---------------- |---------------- |---------------- |---------------- |---------------- |
|    Elapsed Time |              ms |            0.00 |            0.00 |            0.00 |            0.00 |
|[Counter] AddTaskCounter |      operations |          500.00 |          500.00 |          500.00 |            0.00 |

### Per-second Totals
|          Metric |       Units / s |         Max / s |     Average / s |         Min / s |      StdDev / s |
|---------------- |---------------- |---------------- |---------------- |---------------- |---------------- |
|    Elapsed Time |              ms |            0.00 |            0.00 |            0.00 |            0.00 |
|[Counter] AddTaskCounter |      operations |    1,488,981.54 |    1,488,981.54 |    1,488,981.54 |            0.00 |

### Raw Data
#### Elapsed Time
|           Run # |              ms |          ms / s |         ns / ms |
|---------------- |---------------- |---------------- |---------------- |
|               1 |            0.00 |            0.00 |      335,800.00 |

#### [Counter] AddTaskCounter
|           Run # |      operations |  operations / s | ns / operations |
|---------------- |---------------- |---------------- |---------------- |
|               1 |          500.00 |    1,488,981.54 |          671.60 |


## Benchmark Assertions

* [PASS] Expected [Counter] AddTaskCounter to must be greater than or equal to 100.00 operations; actual value was 1,488,981.54 operations.
* [PASS] Expected Elapsed Time to must be less than or equal to 600,000.00 ms; actual value was 0.00 ms.

