﻿# ProjectManagerTest.NBench.TaskTest+NBenchTest+SearchTaskTest
_12/21/2018 8:17:10 PM_
### System Info
```ini
NBench=NBench, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
OS=Microsoft Windows NT 6.2.9200.0
ProcessorCount=4
CLR=4.0.30319.42000,IsMono=False,MaxGcGeneration=2
```

### NBench Settings
```ini
RunMode=Iterations, TestMode=Test
NumberOfIterations=1, MaximumRunTime=00:00:01
Concurrent=False
Tracing=False
```

## Data
-------------------

### Totals
|          Metric |           Units |             Max |         Average |             Min |          StdDev |
|---------------- |---------------- |---------------- |---------------- |---------------- |---------------- |
|    Elapsed Time |              ms |            2.00 |            2.00 |            2.00 |            0.00 |
|[Counter] SearchTaskCounter |      operations |          500.00 |          500.00 |          500.00 |            0.00 |

### Per-second Totals
|          Metric |       Units / s |         Max / s |     Average / s |         Min / s |      StdDev / s |
|---------------- |---------------- |---------------- |---------------- |---------------- |---------------- |
|    Elapsed Time |              ms |        1,176.47 |        1,176.47 |        1,176.47 |            0.00 |
|[Counter] SearchTaskCounter |      operations |      294,117.65 |      294,117.65 |      294,117.65 |            0.00 |

### Raw Data
#### Elapsed Time
|           Run # |              ms |          ms / s |         ns / ms |
|---------------- |---------------- |---------------- |---------------- |
|               1 |            2.00 |        1,176.47 |      850,000.00 |

#### [Counter] SearchTaskCounter
|           Run # |      operations |  operations / s | ns / operations |
|---------------- |---------------- |---------------- |---------------- |
|               1 |          500.00 |      294,117.65 |        3,400.00 |


## Benchmark Assertions

* [PASS] Expected [Counter] SearchTaskCounter to must be greater than or equal to 100.00 operations; actual value was 294,117.65 operations.
* [PASS] Expected Elapsed Time to must be less than or equal to 600,000.00 ms; actual value was 2.00 ms.

