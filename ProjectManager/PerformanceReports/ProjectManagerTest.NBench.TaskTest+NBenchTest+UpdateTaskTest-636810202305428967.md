﻿# ProjectManagerTest.NBench.TaskTest+NBenchTest+UpdateTaskTest
_12/21/2018 8:17:10 PM_
### System Info
```ini
NBench=NBench, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
OS=Microsoft Windows NT 6.2.9200.0
ProcessorCount=4
CLR=4.0.30319.42000,IsMono=False,MaxGcGeneration=2
```

### NBench Settings
```ini
RunMode=Iterations, TestMode=Test
NumberOfIterations=1, MaximumRunTime=00:00:01
Concurrent=False
Tracing=False
```

## Data
-------------------

### Totals
|          Metric |           Units |             Max |         Average |             Min |          StdDev |
|---------------- |---------------- |---------------- |---------------- |---------------- |---------------- |
|    Elapsed Time |              ms |            1.00 |            1.00 |            1.00 |            0.00 |
|[Counter] UpdateTaskCounter |      operations |          500.00 |          500.00 |          500.00 |            0.00 |

### Per-second Totals
|          Metric |       Units / s |         Max / s |     Average / s |         Min / s |      StdDev / s |
|---------------- |---------------- |---------------- |---------------- |---------------- |---------------- |
|    Elapsed Time |              ms |        2,679.53 |        2,679.53 |        2,679.53 |            0.00 |
|[Counter] UpdateTaskCounter |      operations |    1,339,764.20 |    1,339,764.20 |    1,339,764.20 |            0.00 |

### Raw Data
#### Elapsed Time
|           Run # |              ms |          ms / s |         ns / ms |
|---------------- |---------------- |---------------- |---------------- |
|               1 |            1.00 |        2,679.53 |      373,200.00 |

#### [Counter] UpdateTaskCounter
|           Run # |      operations |  operations / s | ns / operations |
|---------------- |---------------- |---------------- |---------------- |
|               1 |          500.00 |    1,339,764.20 |          746.40 |


## Benchmark Assertions

* [PASS] Expected [Counter] UpdateTaskCounter to must be greater than or equal to 100.00 operations; actual value was 1,339,764.20 operations.
* [PASS] Expected Elapsed Time to must be less than or equal to 600,000.00 ms; actual value was 1.00 ms.

